# Copyright 2011 Ingmar Vanhassel
# Copyright 2013 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'luatex-0.65.0.ebuild' which is
#   Copyright 1999-2011 Gentoo Foundation

require texlive-common

SUMMARY="An extended version of pdfTeX using Lua as an embedded scripting language"
DESCRIPTION="
The LuaTeX projects main objective is to provide an open and configurable variant of TeX while at
the same time offering downward compatibility.
"

MY_PNV="${PN}-beta-${PV}"

HOMEPAGE="http://www.luatex.org/"
DOWNLOADS="http://foundry.supelec.fr/frs/download.php/file/15745/${MY_PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/poppler[<0.58]
        dev-libs/kpathsea
        dev-libs/zziplib
        media-libs/libpng:=
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/pixman:1
"

WORK="${WORKBASE}/${MY_PNV}/source/texk/web2c"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-luatex

    --disable-cxx-runtime-hack
    --disable-all-pkgs
    --disable-multiplatform
    --disable-ptex
    --disable-tex
    --disable-mf
    --disable-ipc
    --disable-shared
    --without-mf-x-toolkit
    --without-x

    --with-system-{gd,kpathsea,libpng,poppler,cairo,pixman,t1lib,teckit,xpdf,zlib,zziplib}
)
DEFAULT_SRC_INSTALL_EXCLUDE=( packaging.c )

src_prepare() {
    default
    has_version 'app-text/poppler[>=0.26.0]' \
        && expatch -p4 "${FILES}"/${PN}-poppler26.patch
}

src_configure() {
    default

    #edo cd ../../libs/obsdcompat
    #econf

    edo cd ../../
    edo texk/web2c/luatexdir/getluatexsvnversion.sh
}

src_compile() {
    emake luatex
}

src_install() {
    # some subdirectories lack an 'install' target, so 'make install' aborts
    emake install-exec-am DESTDIR="${IMAGE}" bin_PROGRAMS="luatex" # SUBDIRS="" nodist_man_MANS=""

    # these symlinks are handled by texlive-core TODO proper auto-toolisation
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/{dvitomp,mfplain}

    emagicdocs

    emake -C man luatex.1
    doman man/luatex.1
}

pkg_postinst() {
    efmtutil-sys
}


