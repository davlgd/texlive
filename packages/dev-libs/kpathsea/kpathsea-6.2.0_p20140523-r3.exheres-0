# Copyright 2011 Ingmar Vanhassel
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in part upon kpathsea-6.0.0_p20100722.ebuild from Gentoo, which is:
# Copyright 1999-2010 Gentoo Foundation

require texlive-common

SUMMARY="A library to do path searching, mainly for TeX"
HOMEPAGE="http://tug.org/kpathsea/"
DOWNLOADS="
    mirror://ctan/systems/texlive/Source/texlive-${PV#*_p}-source.tar.xz
    https://dev.exherbo.org/distfiles/texlive/2014/texlive-module-${PN}-2014.tar.xz
    doc? ( https://dev.exherbo.org/distfiles/texlive/2014/texlive-module-${PN}.doc-2014.tar.xz )
    https://dev.exherbo.org/distfiles/texlive/2013/kpathsea-texmf.d-5.tar.xz
"

LICENCES="|| ( LGPL-3 LGPL-2.1 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc source static"

DEPENDENCIES="
"

require texlive-common

WORK=${WORKBASE}/texlive-${PV#*_p}-source/texk/${PN}

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( static )

src_prepare() {
    edo cd "${WORKBASE}/texlive-${PV#*_p}-source"
    default
}

src_install() {
    default

    # The default configuration expects it to be world writable, Gentoo #266680
    keepdir /var/cache/fonts/
    edo chmod 1777 "${IMAGE}"/var/cache/fonts

    # SELFAUTODIR doesn't give the right value for multiarch in
    # the case of TEXMFROOT
    # For TEXMFSYSVAR and TEXMFSYSCONFIG we strip the SELFAUTOPARENT
    # since it would result in /usr/{var,etc}/...
    edo sed -e "/TEXMFROOT/s:\$SELFAUTODIR:\$SELFAUTOPARENT:" \
            -e '/^TEXMFSYS/s:\$SELFAUTOPARENT::' \
            -i "${WORKBASE}"/texmf.d/05searchpaths.cnf

    keepdir /etc/texmf/{fmtutil.d,texmf.d}
    insinto /etc/texmf/texmf.d/
    doins "${WORKBASE}"/texmf.d/*.cnf

    edo rm -f "${WORK}"/texmf-dist/web2c/texmf.cnf
    edo rm -f "${WORK}"/texmf-dist/web2c/fmtutil.cnf

    keepdir /var/lib/texmf

    # texmf-update fails if this dir does not exist
    keepdir /etc/texmf/web2c/

    if optionq source; then
        edo cp -pPR "${WORKBASE}"/tlpkg "${IMAGE}/usr/share"
    fi

    dosym /etc/texmf/web2c/fmtutil.cnf /usr/share/texmf-dist/web2c/fmtutil.cnf
    dosym /etc/texmf/web2c/texmf.cnf /usr/share/texmf-dist/web2c/texmf.cnf

    # these are handled by texlive-basic
    nonfatal edo rm "${IMAGE}"/usr/share/texmf-dist/web2c/mktex*

    dobin "${FILES}/texmf-update"

    emagicdocs
}

pkg_postrm() {
    etexmf-update
}

pkg_postinst() {
    etexmf-update
}

